package com.oporazita.travelcard;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    EditText busID, passengerID;
    String strBusID, strPassengerID;
    Button submitButton;
    String date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        busID = findViewById(R.id.bus_ID);
        passengerID = findViewById(R.id.passenger_ID);

        submitButton = findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strBusID = busID.getText().toString();
                strPassengerID = passengerID.getText().toString();
                date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                Toast.makeText(getApplicationContext(), "Bus Id: " + strBusID + "\nPassanger ID: " + strPassengerID + "\nDate: " + date, Toast.LENGTH_LONG).show();
                new updateData().execute();
            }
        });
    }

    public class updateData extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            RequestBody body = new FormBody.Builder()
                    .add("busid", strBusID)
                    .add("passengerid", strPassengerID)
                    .add("date",date)
                    .build();
            Request request = new Request.Builder()
                    .url("https://www.oporazita.com/travelcard/data_entry.php")
                    .post(body)
                    .build();
            try {
                client.newCall(request).execute();
                //Toast.makeText(getApplicationContext(),"Updated!",Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
